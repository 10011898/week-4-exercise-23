﻿using System;
using System.Collections.Generic;

namespace week_4_exercise_23
{
    class Program
    {
        static void Main(string[] args)
        {
            var movies = new Dictionary<string ,string>();
            movies.Add("Avengers","Action");
            movies.Add("WallE","Animated");
            movies.Add("IP Man","Action");
            movies.Add("Inception","Thriller");
            movies.Add("Hannibal","Horror");

            var output = "";
            var value  = movies.TryGetValue("Action", out output);
            
            foreach(var one in movies)
            {
                if(one.Value == "Action")
                {
                    Console.WriteLine($"{one.Key} is an {one.Value} movie");
                }
            }
            Console.WriteLine();

            Console.WriteLine("Amount of movies that are action:");
            var count = 0;
            var action = new List<string>();
            foreach(var two in movies)
            {
                if(two.Value == "Action")
                {
                    count++;
                    action.Add(two.Key);
                }
            }
            Console.WriteLine(count);
            Console.WriteLine();

            var end = string.Join(" & ",action);
            Console.WriteLine($"{end} are the movies with an action genre.");
        }
    }
}
